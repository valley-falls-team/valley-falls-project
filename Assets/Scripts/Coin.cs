﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinValue;
	private bool _collected = false;
    public Animator animator;

    
    private PlayerMovement player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag=="Player"){
			if(!_collected){
				player.AddValues(coinValue);
				_collected = true;
                animator.SetTrigger("Collect");
                FindObjectOfType<AudioManager>().Play("coin_04");
                Destroy(gameObject, 0.5f);
			}
		}
	}
}
