﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    public Slider slider;

    public void setMaxHealth(float value)
    {
        slider.maxValue = value;
        slider.value = value;
    }

    public void setHealth(float value)
    {
        slider.value = value;
    }
    
}
