﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderVictory : MonoBehaviour
{

    private PlayerMovement player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag=="Player"){
			player.hasWon = true;
            FindObjectOfType<AudioManager>().Stop("Abandoned_Village_Short");
            FindObjectOfType<AudioManager>().Play("Final_Glorious_Triumph__10s");


        }
	}
}
