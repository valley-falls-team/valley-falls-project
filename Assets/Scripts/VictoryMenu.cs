﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryMenu : MonoBehaviour
{

    public GameObject victoryMenuUI;
    private PlayerMovement player;

    void Start()
    {
        Time.timeScale = 1f;
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.hasWon)
        {
            Time.timeScale = 0f;
            victoryMenuUI.SetActive(true);
        }
    }

    public void LoadGameMenu()
    {
        
        SceneManager.LoadScene("Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
