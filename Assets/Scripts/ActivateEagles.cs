﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateEagles : MonoBehaviour
{
    public GameObject eagles;

    private PlayerMovement player;
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag=="Player"){
            eagles.SetActive(true);


        }
	}
}
