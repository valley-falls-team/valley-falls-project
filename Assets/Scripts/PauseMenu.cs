﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;
    public GameObject pauseMenuUI;
    
    //string menu = SceneManager.GetSceneAt(0).name;

	

    // Update is called once per frame
    void Update()
    {
      if(Input.GetButtonDown("Cancel"))
        {
        if(isPaused){
          ResumeGame();
        }
        else{
          pauseMenuUI.SetActive(true);
          Time.timeScale=0f;
          isPaused=true;
        }
      }
    }
    public void ResumeGame(){
      pauseMenuUI.SetActive(false);
      Time.timeScale=1f;
      isPaused=false;
    }

    public void LoadGameMenu(){
      SceneManager.LoadScene("Menu");
    }

    public void QuitGame(){
      Application.Quit();
    }
}
