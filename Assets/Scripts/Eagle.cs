﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Eagle : MonoBehaviour 
{
    public Animator animator;
    public float maxHealth = 10f;
    public float currentHealth;
    public LayerMask playerLayer;
    public Transform attackPoint;
    public Transform target;
    public float attackRange = 0.5f;
    public float attackDamage = 5f;
    public float attackRate = 0.5f;
    private float nextAttTime = 0f;
    private bool inRange = false;
    private bool m_FacingRight = false; 
    private int inRangeS = 0;
    private bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
        if (m_FacingRight && target.transform.position.x <= this.transform.position.x)
        {
            Flip();
        }
        else if (!m_FacingRight && target.transform.position.x >= this.transform.position.x)
        {
            Flip();
        }
        if (Vector2.Distance(attackPoint.transform.position, target.transform.position) <= attackRange && !isDead)
        {
            inRangeS++;
            if (Time.time >= nextAttTime && !target.GetComponent<PlayerMovement>().isDead && inRangeS > 3)
            {
                Attack();
                nextAttTime = Time.time + (1f / attackRate);
            }

        }
        else
        {
            inRangeS = 0;
        }

    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        
        nextAttTime = Time.time + (1f / attackRate);
        Hurt();
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        isDead = true;
        FindObjectOfType<AudioManager>().Play("explosion_10");
        GetComponent<CircleCollider2D>().enabled = false;
        Destroy(gameObject, 0.5f);
        this.enabled = false;
        
    }

    public void Attack()
    {
        
        animator.SetTrigger("Attack");
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);
        FindObjectOfType<AudioManager>().Play("hawk");
        foreach (Collider2D layer in hitPlayer)
        {
            layer.GetComponent<PlayerMovement>().TakeDamage(attackDamage);
        }

    }


    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void Hurt()
    {
        animator.SetTrigger("Hurt");
        
    }


}
