﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int healthValue;
    private bool _collected = false;
    public Animator animator;


    private PlayerMovement player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (!_collected)
            {
                player.Heal(healthValue);
                _collected = true;
                animator.SetTrigger("Collect");
                FindObjectOfType<AudioManager>().Play("power_up_10");
                Destroy(gameObject, 0.5f);
            }
        }
    }
}
