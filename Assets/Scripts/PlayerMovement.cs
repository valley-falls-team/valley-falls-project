﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;



public class PlayerMovement : MonoBehaviour
{

    public BasicMovement controller;
    public Animator animator;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public HealthBar hb;

    public int bodovi;

    public Text uiText;
    

    [SerializeField] private float runSpeed = 40f;
    public float attackDamage = 20f;
    public float attackRate = 2f;
    private float nextAttTime = 0f;
    public float maxHealth = 100f;
    public float currentHealth;
    public bool isDead = false;
    float horizontalMove = 0f;
    bool jump = false;
    public bool hasWon = false;

    AudioSource walking;
    bool playWalking;
    bool isPlaying;

    void Start()
    {
        currentHealth = maxHealth;
        hb.setMaxHealth(maxHealth);
        walking = GetComponent<AudioSource>();
        isPlaying = false;
        uiText.text="Score: " + bodovi;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if(Time.time >= nextAttTime)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Attack();
                nextAttTime = Time.time + 0.5f + (1f / attackRate);
            }
        }

        if (animator.GetFloat("Speed") > 0.1 && controller.m_Grounded)
        {
            playWalking = true;
        }
        else
        {
            playWalking = false;
        }

        if (playWalking && !isPlaying)
        {
            walking.Play();
            isPlaying = true;
        }

        if (!playWalking && isPlaying)
        {
            walking.Stop();
            isPlaying = false;
        }

    }

    public void Attack()
    {
        animator.SetBool("IsJumping", false);
        animator.SetTrigger("Attack");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
        FindObjectOfType<AudioManager>().Play("Sword Swish 1");

        foreach (Collider2D enemy in hitEnemies)
        {
            try
            {
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            } catch (NullReferenceException ex)
            {
                enemy.GetComponent<Eagle>().TakeDamage(attackDamage);
            }

        }
        if (!controller.m_Grounded)
            animator.SetBool("IsJumping", true);

    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsTag("1"))
        {
            horizontalMove = 0;
        }
        controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
        if(!controller.m_Grounded)
            animator.SetBool("IsJumping", true);
        jump = false;
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        animator.SetTrigger("Hit");
        
        if (currentHealth <= 0)
        {
            hb.setHealth(0f);
            Die();
        } else
        {
            hb.setHealth(currentHealth);
        }
    }

    public void Die()
    {
        isDead = true;
        animator.SetTrigger("Death");
        animator.SetBool("IsDead", true);

        FindObjectOfType<AudioManager>().Stop("Abandoned_Village_Short");

        FindObjectOfType<AudioManager>().Play("Battle_Lost_6s");
        return;
    }

    public void AddValues(int coinValue){
        bodovi += coinValue;
		uiText.text="Score: " + bodovi;
    }

    public void Heal(int healthValue)
    {
        currentHealth += healthValue;
        if (currentHealth >= 100)
        {
            currentHealth = 100f;
            hb.setHealth(100f);
        }
        else
        {
            hb.setHealth(currentHealth);
        }
    }



}
