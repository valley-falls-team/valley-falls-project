﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
    public Animator animator;
    public float maxHealth = 100f;
    public float currentHealth;
    public LayerMask playerLayer;
    public Transform attackPoint;
    public Transform target;
    public Transform patrolPointLeft, patrolPointRight;
    public float attackRange = 0.5f;
    public float range = 20f;
    public float attackDamage = 20f;
    public float attackRate = 0.5f;
    private float nextAttTime = 0f;
    private bool inRange = false;
    public Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = false; 
    private Vector3 m_Velocity = Vector3.zero;
    private float m_MovementSmoothing = .05f;
    public float speed = 4f;
    private int inRangeS = 0;
    private bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
        if (Vector2.Distance(attackPoint.transform.position, target.transform.position) <= attackRange && !isDead)
        {
            inRangeS++;
            animator.SetInteger("AnimState", 1);
            if (Time.time >= nextAttTime && !target.GetComponent<PlayerMovement>().isDead && inRangeS > 8)
            {
                Attack();
                nextAttTime = Time.time + (1f / attackRate);
            }

        }
        else if (Vector2.Distance(attackPoint.transform.position, target.transform.position) <= range &&
            target.transform.position.x >= patrolPointLeft.transform.position.x &&
            target.transform.position.x <= patrolPointRight.transform.position.x &&
            !isDead)
        {
            inRangeS = 0;
            animator.SetInteger("AnimState", 2);
            MoveToAttack();
        }
        else if(!isDead)
        {
            inRangeS = 0;
            animator.SetInteger("AnimState", 2);
            Patrol();
        }

    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        FindObjectOfType<AudioManager>().Play("Sword 3");

        nextAttTime = Time.time + (1f / attackRate);
        if (currentHealth <= 0)
        {
            Die();
        }
        else
        {
            animator.SetTrigger("Hurt");
        }
    }

    public void Die()
    {
        isDead = true;
        animator.SetTrigger("Death");
        GetComponent<Collider2D>().enabled = false;
        //GetComponent<CircleCollider2D>().enabled = false;
        //gameObject.transform.GetComponent<Rigidbody>().bodyType = RigidbodyType2D.Static;
        Destroy(gameObject, 3.5f);

    }

    public void Attack()
    {
        
        animator.SetTrigger("Attack");
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);
        FindObjectOfType<AudioManager>().Play("Sword 5");
        foreach (Collider2D layer in hitPlayer)
        {
            layer.GetComponent<PlayerMovement>().TakeDamage(attackDamage);
        }
        
    }

    public void Patrol()
    {
        if (m_FacingRight)
        {
            Vector3 targetVelocity = new Vector2(0.7f * speed, m_Rigidbody2D.velocity.y);
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            if(patrolPointRight.transform.position.x <= this.transform.position.x)
            {
                Flip();
            }
        }
        else
        {
            Vector3 targetVelocity = new Vector2(-0.7f * speed, m_Rigidbody2D.velocity.y);
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            if (patrolPointLeft.transform.position.x >= this.transform.position.x)
            {
                Flip();
            }
        }
    }

    public void MoveToAttack()
    {
        if (m_FacingRight && target.transform.position.x <= this.transform.position.x)
        {
            Flip();
        } else if (!m_FacingRight && target.transform.position.x >= this.transform.position.x)
        {
            Flip();
        }
        if (m_FacingRight)
        {
            Vector3 targetVelocity = new Vector2(1.5f * speed, m_Rigidbody2D.velocity.y);
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            
        }
        else
        {
            Vector3 targetVelocity = new Vector2(-1.5f * speed, m_Rigidbody2D.velocity.y);
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            
        }

    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


}
