﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{

    public GameObject deathMenuUI;
    private PlayerMovement player;

    void Start()
    {
        Time.timeScale = 1f;
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.isDead)
        {
            Time.timeScale = 0f;
            deathMenuUI.SetActive(true);
        }
    }

    public void LoadGameMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ResetLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
